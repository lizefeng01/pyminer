import os
import sys
import qdarkstyle
from PyQt5.QtCore import Qt, QUrl
from PyQt5.Qt import *
from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtWidgets import QWidget, QDesktopWidget, QFileDialog, QApplication, QDialog
from pyminer2.ui.base.option import Ui_Form as Option_Ui_Form


class OptionForm(QWidget, Option_Ui_Form):
    """
    打开"选项"窗口
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.center()
        # QssTools.set_qss_to_obj(root_dir + "/ui/source/qss/pyminer.qss", self)

        # 通过combobox控件选择窗口风格
        self.comboBox_theme.activated[str].connect(self.change_theme)

        self.setting = dict()

        self.listWidget.currentRowChanged.connect(self.option_change)
        self.toolButton_workspace.clicked.connect(self.slot_change_workspace)
        self.toolButton_output.clicked.connect(self.slot_change_output)
        self.pushButton_cancel.clicked.connect(self.close)
        self.pushButton_ok.clicked.connect(self.close)

    def keyPressEvent(self, e):
        """
        按键盘Escape退出当前窗口
        @param e:
        """
        if e.key() == Qt.Key_Escape:
            self.close()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def option_change(self, i):
        self.stackedWidget.setCurrentIndex(i)

    def change_theme(self, style):
        from pyminer2.pmutil import get_main_window

        app=QApplication.instance()

        if style == 'Fusion':
            get_main_window().settings['main_theme']='#F7F7F7'
            get_main_window().settings['margin_theme']='#DADADA'
            app.setStyleSheet('')
            get_main_window().load_stylesheet('standard')
            # app.setStyle('Fusion')

        elif style == 'Qdarkstyle':
            get_main_window().setStyleSheet('')
            app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
            black_ss = """
            QPushButton#pmtopToolbarButton[stat="selected"]{
    /*背景颜色*/
    height:20px;
    padding:0px 0px 0px 0px;
    border:1px;
    border-radius:8px;
    background-color:#555555;
}
QPushButton#pmtopToolbarButton[stat="unselected"]{
    /*背景颜色*/
    height:20px;
    padding:0px 0px 0px 0px;
    border:1px;
    border-radius:8px;
    background-color:#222222;
}
QToolBar#pmTopToolBar{height:25px;}
            """
            app.setStyleSheet(app.styleSheet()+'\n'+black_ss)

        elif style.lower() == 'windowsvista':
            app.setStyle("windowsvista")
        elif style.lower() == 'windows':
            app.setStyle("Windows")

    def slot_change_workspace(self):
        directory = QFileDialog.getExistingDirectory(self, "选择工作区间位置", os.path.expanduser('~'))
        self.lineEdit_workspace.setText(directory)

    def slot_change_output(self):
        directory = QFileDialog.getExistingDirectory(self, "选择输出文件夹位置", os.path.expanduser('~'))
        self.lineEdit_output.setText(directory)


class AppstoreForm(QMainWindow):
    def __init__(self):
        super(QMainWindow, self).__init__()
        self.center()
        self.setWindowTitle('应用商店')
        self.setGeometry(5, 30, 1355, 730)

        self.browser = QWebEngineView()
        # 加载外部的web界面
        self.browser.load(QUrl('https://chrome.zzzmh.cn/index#ext'))
        self.setCentralWidget(self.browser)

    def keyPressEvent(self, e):
        """
        按键盘Escape退出当前窗口
        @param e:
        """
        if e.key() == Qt.Key_Escape:
            self.close()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

if __name__ == '__main__':
    app=QApplication(sys.argv)
    form=AppstoreForm()
    form.show()
    sys.exit(app.exec())