from typing import List, Dict, Tuple

from PyQt5.QtGui import QCloseEvent, QColor
from PyQt5.QtWidgets import QWidget, QPushButton, QLineEdit, QLabel, QVBoxLayout, QHBoxLayout, QApplication, \
    QColorDialog, QRadioButton, QCheckBox, QComboBox, QSpacerItem, QSizePolicy
import string
import sys


class BaseParamWidget(QWidget):
    '''
    基础参数控件的类型。所有的参数控件都在其上派生而来。
    '''

    def __init__(self, layout_dir='v'):
        super(BaseParamWidget, self).__init__()
        if layout_dir == 'v':
            self.central_layout = QVBoxLayout()
        else:
            self.central_layout = QHBoxLayout()
        self.setLayout(self.central_layout)
        self.central_layout.setContentsMargins(0, 0, 0, 0)

        self.on_para_change = None
        self.__app = None  # SciApp。初始化控件的时候指定，并且在调用set_app的时候传入。

    def para_changed(self):
        if (self.on_para_change is not None) and (self.__app is not None):
            self.on_para_change(self.__app)

    def set_app(self, app):
        self.__app = app

    def is_key(self, event, type=''):
        '''
        'dir':判断方向键
        'alpha':判断是否为26个字母
        'hex':判断是否为十六进制数字或者字母
        'digit':判断是否为数字0~9
        'valid':包含数字、字母或者退格键。
        '''

        type = type.lower()
        if type == '':
            return True
        elif type.startswith('dir'):
            return event.keysym.lower() in ('left', 'right', 'up', 'down')
        elif type.startswith('alpha'):
            return event.keysym in string.ascii_lowercase
        elif type.startswith('hex'):
            return event.keysym in string.hexdigits
        elif type.startswith(('digit')):
            return event.keysym in string.digits

    def set_value(self):
        pass

    def get_value(self):
        pass

    def set_params(self, *args, **kwargs):
        pass


class NumCtrl(BaseParamWidget):
    """NumCtrl: derived from tk.Entry
    用于输入数值。
    """

    def __init__(self, layout_dir: str, title: str, initial_value: int, unit: str, rang: tuple):
        super().__init__(layout_dir=layout_dir)
        self.on_check_callback = None

        self.prefix = QLabel(text=title)
        entryLayout = QHBoxLayout()
        entryLayout.setContentsMargins(0, 0, 0, 0)

        self.ctrl = QLineEdit()
        self.ctrl.textChanged.connect(self.ontext)

        self.postfix = QLabel(text=unit)

        self.central_layout.addWidget(self.prefix)
        self.central_layout.addLayout(entryLayout)
        entryLayout.addWidget(self.ctrl)
        entryLayout.addWidget(self.postfix)

        self.min, self.max = rang
        self.accury = initial_value
        self.set_value(initial_value)

    def Bind(self, z, f):
        self.f = f

    def ontext(self, event):
        self.f(self)
        if self.get_value() is None:
            self.ctrl.setStyleSheet("background-color:#ff0000;")
        else:

            self.ctrl.setStyleSheet("background-color:#ffffff;")
            self.para_changed()
        self.ctrl.update()
        if callable(self.on_check_callback):
            self.on_check_callback()

    def set_value(self, n):
        self.ctrl.clear()
        self.ctrl.setText(str(round(n, self.accury) if self.accury > 0 else int(n)))

    def get_value(self):
        sval = self.ctrl.text()
        try:
            num = float(sval) if self.accury > 0 else int(sval)
        except ValueError:
            import traceback
            traceback.print_exc()
            return None
        if num < self.min or num > self.max:
            return None
        if abs(round(num, self.accury) - num) > 10 ** -(self.accury + 5):  # 这么写才比较严谨吧
            return None
        return num

    def f(self, e):
        pass

    def Refresh(self):
        pass


class TextCtrl(BaseParamWidget):
    def __init__(self, layout_dir: str, title: str, initial_value: str):
        super().__init__(layout_dir)
        self.on_check_callback = None

        self.prefix = QLabel(text=title)

        entryLayout = QHBoxLayout()
        entryLayout.setContentsMargins(0, 0, 0, 0)
        self.ctrl = QLineEdit()
        self.ctrl.textChanged.connect(self.ontext)

        # self.postfix = lab_unit = QLabel(text=unit)

        self.central_layout.addWidget(self.prefix)
        self.central_layout.addLayout(entryLayout)
        entryLayout.addWidget(self.ctrl)
        self.set_value(initial_value)
        # entryLayout.addWidget(self.postfix)

    def param_changed(self, event):
        pass

    # ! TODO: what is this?
    def Bind(self, z, f):
        self.f = f

    def ontext(self, event):
        self.para_changed()

    def set_value(self, text: str):
        self.ctrl.clear()
        self.ctrl.setText(text)

    def get_value(self) -> str:
        return self.ctrl.text()


class ColorCtrl(BaseParamWidget):
    def __init__(self, layout_dir: str, title: str, initial_value: str):
        super().__init__(layout_dir)
        self.on_check_callback = None
        self.prefix = QLabel(text=title)

        entryLayout = QHBoxLayout()

        self.ctrl = QLineEdit()
        self.ctrl.textChanged.connect(self.ontext)

        self.color_button = QPushButton()
        self.color_button.clicked.connect(self.oncolor)

        # self.postfix = lab_unit = QLabel(text=unit)
        self.central_layout.addWidget(self.prefix)
        self.central_layout.addLayout(entryLayout)
        entryLayout.addWidget(self.ctrl)
        entryLayout.addWidget(self.color_button)
        # entryLayout.addWidget(self.postfix)
        self.set_value(initial_value)

    def Bind(self, z, f):
        self.f = f

    def ontext(self, event):
        if self.get_value() is None:
            self.color_button.setStyleSheet("background-color:#ff0000;")
            self.ctrl.setStyleSheet("background-color:#ff0000;")
        else:
            self.ctrl.setStyleSheet('background-color:#ffffff;')
            self.color_button.setStyleSheet("background-color:%s;" % self.colorTup2Str(self.get_value()))
            self.para_changed()
        self.ctrl.update()
        if callable(self.on_check_callback):
            self.on_check_callback()

    def oncolor(self, event):
        color = QColorDialog.getColor(initial=QColor(*self.get_value()))
        self.set_value(self.colorStr2Tup(color.name()))
        if callable(self.on_check_callback):
            self.on_check_callback()

    def set_value(self, color: Tuple = None):
        if color is None:
            color = (255, 255, 255)
        strcolor = self.colorTup2Str(color)
        self.color_button.setStyleSheet('background-color:%s;' % strcolor)
        self.ctrl.clear()
        self.ctrl.setText(strcolor)

    def get_value(self):
        rgb = self.ctrl.text().strip()
        if len(rgb) != 7 or rgb[0] != '#':
            return None
        try:
            int(rgb[1:], 16)
        except:
            import traceback
            traceback.print_exc()
            return None
        return self.colorStr2Tup(rgb)

    def colorStr2Tup(self, value: str) -> tuple:  # pos或者wh的输入都是tuple
        def convert(c):
            v = ord(c)
            if (48 <= v <= 57):
                return v - 48
            else:
                return v - 87  # 返回a的值。

        value = value.lower()
        c0 = convert(value[1])
        c1 = convert(value[2])
        c2 = convert(value[3])
        c3 = convert(value[4])
        c4 = convert(value[5])
        c5 = convert(value[6])
        a1 = c0 * 16 + c1
        a2 = c2 * 16 + c3
        a3 = c4 * 16 + c5
        return (a1, a2, a3)

    def colorTup2Str(self, value: tuple) -> str:
        if value is None:
            return None
        strcolor = '#'
        for i in value:
            strcolor += hex(int(i))[-2:].replace('x', '0')
        return strcolor


class PathCtrl(BaseParamWidget):
    def __init__(self, layout_dir: str, parent, title, filt):
        super().__init__(layout_dir)
        self.prefix = lab_title = QLabel(text=title)
        path_layout = QHBoxLayout()
        path_layout.addWidget(lab_title)

        self.ctrl = QLineEdit()
        path_layout.addWidget(self.ctrl)
        self.file_choose_button = QPushButton('..')
        path_layout.addWidget(self.file_choose_button)
        self.central_layout.addLayout(path_layout)

    def ontext(self, event):
        self.para_changed()

    def onselect(self, event):
        # [TODO]:对应的这个函数是啥意思？一定要注意一下！
        pass

    def set_value(self, value: str):
        pass

    def get_value(self) -> str:
        pass


class Choice(BaseParamWidget):
    def __init__(self, layout_dir: str, choices, tp, title, unit):
        super().__init__(layout_dir)
        self.tp, self.choices = tp, choices
        self.on_check_callback = None

        self.prefix = QLabel(self, text=title)
        self.central_layout.addWidget(self.prefix)
        self.radio_buttons: List['QRadioButton'] = []
        for i, choice in enumerate(self.choices):
            b = QRadioButton(text=str(choice))
            b.toggled.connect(self.on_radio_button_toggled)
            self.radio_buttons.append(b)
            self.central_layout.addWidget(b)

        self.postfix = QLabel(text=unit)
        self.central_layout.addWidget(self.postfix)

    def on_radio_button_toggled(self):
        sender: QRadioButton = self.sender()
        if sender.isChecked():
            # index = self.radio_buttons.index(sender)
            pass

    def on_choice(self, event=None):
        # attention : button command will not transfer any event as args .
        # 注意：按钮本身并不会传递event作为参数，与键鼠的event不同。
        self.f(self)
        self.para_changed()
        if callable(self.on_check_callback):
            self.on_check_callback()

    def set_value(self, x):
        for i, choice in enumerate(self.choices):
            if x == choice:
                self.radio_buttons[i].setChecked(True)
            else:
                self.radio_buttons[i].setChecked(False)

    def get_value(self):
        for i, radio_button in enumerate(self.radio_buttons):
            if radio_button.isChecked():
                return self.choices[i]


class Check(BaseParamWidget):
    '''
    bool, 'sport', 'do you like sport',True
    '''

    def __init__(self, layout_dir: str, title: str, initial_value: bool):
        super().__init__(layout_dir)
        QLabel(text=title)
        layout = QHBoxLayout()
        self.on_check_callback = None
        check = QCheckBox(text=title)
        check.clicked.connect(self.on_check)
        layout.addWidget(check)
        self.check = check
        self.central_layout.addLayout(layout)
        self.set_value(initial_value)

    def get_value(self):
        return self.check.isChecked()

    def set_value(self, value: bool):
        self.check.setChecked(value)

    def on_check(self):
        pass


class ChoiceBoxCtrl(BaseParamWidget):
    def __init__(self, layout_dir: str, title: str, initial_value: object, choices: list, texts=None):

        super().__init__(layout_dir)
        self.choices = []
        self.text_list = []

        lab_title = QLabel(text=title)
        # layout_v = QVBoxLayout()
        layout = QHBoxLayout()
        self.central_layout.addWidget(lab_title)
        # layout_v.addLayout(layout)
        self.on_check_callback = None
        check = QComboBox()

        check.currentIndexChanged.connect(self.on_value_changed)
        layout.addWidget(check)
        self.central_layout.addLayout(layout)
        self.check = check
        # self.central_layout.addLayout(layout_v)
        self.set_choices(choices, texts)
        self.set_value(initial_value)

    def set_choices(self, choices: list, texts: list = None):
        self.check.clear()
        self.choices = choices
        self.text_list = []
        if texts is None:
            for choice in choices:
                self.text_list.append(str(choice))
        else:
            if len(texts) != len(choices):
                raise Exception('Length of argument \'choices\'(len=%d) and \'texts\'(len=%d) are not same!' %
                                (len(choices), len(texts)))
            else:
                self.text_list = texts
        self.check.addItems(self.text_list)

    def on_value_changed(self):
        pass

    def set_value(self, value: object):
        index = self.choices.index(value)
        self.check.setCurrentIndex(index)

    def get_value(self):
        return self.choices[self.check.currentIndex()]


views_dic = {str: TextCtrl, int: NumCtrl, float: NumCtrl, bool: Check, list: Choice}
views_dic.update({'choose_box': ChoiceBoxCtrl, 'color': ColorCtrl, 'number': NumCtrl
                     , 'line_edit': TextCtrl, 'bool': Check})


class SettingsPanel(QWidget):
    widgets_dic: Dict[str, BaseParamWidget] = {}

    def __init__(self, parent=None, views: List[Tuple[str]] = None, layout_dir: str = 'v'):
        super(SettingsPanel, self).__init__(parent)
        self.layout_dir = layout_dir
        if layout_dir == 'v':
            self.setLayout(QVBoxLayout())
        else:
            self.setLayout(QHBoxLayout())

        self.set_items(views)

    def _set_items(self, views: List[Tuple[str]] = None):
        if views is None:
            return
        self.widgets_dic: Dict[str, QWidget] = {}
        self.layout().setContentsMargins(0, 0, 0, 0)
        for v in views:
            name = v[1]
            widget = views_dic[v[0]](self.layout_dir, *v[2:])
            if self.widgets_dic.get(name) is None:
                self.widgets_dic[name] = widget
            self.layout().addWidget(widget)
        self.layout().addItem(QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding))

    def set_items(self, items: List[Tuple[str]] = None):
        self.widgets_dic = {}
        for i in range(self.layout().count()):
            item = self.layout().itemAt(i).widget()
            if item is not None:
                item.deleteLater()
        self._set_items(items)

    def get_ctrl(self, ctrl_name: str):
        return self.widgets_dic.get(ctrl_name)

    def get_value(self):
        result = {}
        for k in self.widgets_dic:
            result[k] = self.widgets_dic[k].get_value()
        return result

    def closeEvent(self, a0: QCloseEvent) -> None:
        super().closeEvent(a0)
        print(self.get_value())
        self.deleteLater()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    # 类型；名称；显示的提示文字;初始值；//单位；范围
    views = [('line_edit', 'name', 'What\'s your name?', 'hzy'),
             ('number', 'age', 'How old are you?', 88, 'years old', (0, 150)),
             ('number', 'height', 'How High could This Plane fly?', 12000, 'm', (10, 20000)),
             ('bool', 'sport', 'do you like sport', True),
             ('choose_box', 'plane_type', 'plane type', 'f22', ['f22', 'f18', 'j20', 'su57'],
              ['f22战斗机', 'f18战斗轰炸机', 'j20战斗机', 'su57战斗机']),
             ('color', 'color', 'Which color do u like?', (0, 200, 0))]
    sp = SettingsPanel(views=views, layout_dir='v')
    sp.widgets_dic['plane_type'].set_choices(['aaa', 'vvvvv', 'xxxxxx'])
    sp.set_items(views[3:6])
    sp.show()
    sp2 = SettingsPanel(views=views, layout_dir='h')
    sp2.show()
    sp2.setMaximumHeight(30)
    val = sp.get_value()  # 返回一个字典。初始值为表格的第二列：第四列。
    print(val)
    # root.mainloop()
    sys.exit(app.exec_())
