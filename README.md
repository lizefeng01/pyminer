# PyMiner

[![Build Status](https://travis-ci.com/akizunx/TinyControl.svg?branch=master)](https://travis-ci.com/akizunx/TinyControl)
[![License](https://img.shields.io/badge/license-GPL-blue)](https://img.shields.io/badge/license-GPL-blue)
[![Stars](https://gitee.com/py2cn/pyminer/badge/star.svg?theme=gvp)](https://gitee.com/py2cn/pyminer/stargazers)
[![Platform](https://img.shields.io/badge/python-v3.8-blue)](https://img.shields.io/badge/python-v3.7-blue)
[![Platform](https://img.shields.io/badge/PyQt5-v5.15.0-blue)](https://img.shields.io/badge/PyQt5-v5.15.0-blue)

<center>
    <a src="https://img.shields.io/badge/QQ%e7%be%a4-orange">
        <img src="https://img.shields.io/badge/QQ%e7%be%a4-945391275-orange">
    </a>
    <a src="https://img.shields.io/badge/QQ-454017698-orange">
        <img src="https://img.shields.io/badge/QQ-454017698-orange">
    </a>
</center>

#### 简介
PyMiner一款基于数据工作空间的数学工具，通过加载插件的方式实现不同的需求，用易于操作的形式完成数学计算相关工作。



#### 技术说明

项目开发环境支持跨平台，（windows,linux,mac 都支持！如果使用出现问题，欢迎提issue），建议使用Python3.8+PyQt5.15进行开发。

#### Windows安装

```bash
#第一步：下载源代码
git clone https://gitee.com/py2cn/pyminer.git
#第二步：同步安装依赖包和PyMiner，如果遇到安装失败的情况需要手动安装
python -m pip install -i https://mirrors.cloud.tencent.com/pypi/simple -r requirements.txt
#第三步：运行主程序
python app2.py
```

#### Linux安装

```bash
#第一步：下载源代码
git clone https://gitee.com/py2cn/pyminer.git
#第二步：同步安装依赖包和PyMiner，如果遇到安装失败的情况需要手动安装
python3 -m pip install -i https://mirrors.cloud.tencent.com/pypi/simple -r requirements_linux.txt
#第三步：运行主程序
python3 app2.py
```

#### 如何贡献

1. 在[PyMiner主页](https://gitee.com/py2cn/pyminer)点击Fork，生成你自己的PyMiner库，生成成功之后可以在路径`https://gitee.com/your_name/pyminer`下看到；

2. 将你自己的PyMiner库下载到你的本地：

   ```bash
   git clone https://gitee.com/your_name/pyminer.git && cd pyminer/
   ```

3. 首先确认库的远程链接情况，一般刚克隆下来的库都需要执行该操作，否则会影响Pull Request的提交：

   ```bash
   #执行以下指令确认远程链接情况
   git remote -v
   #如果未链接到远程仓库，则需要执行以下指令
   git remote add upstream https://gitee.com/py2cn/pyminer.git
   #执行成功后，再执行如下指令，应该可以看到4行链接
   git remote -v
   ```

4. 在本地创建一个依照特性来命名的分支，如修改`README.md`：

   ```bash
   git checkout -b modify_readme
   ```

5. 在开发前需要注意编码规范，如PEP8等，这里我们建议提交者使用flake8进行自我校验，确保统一规范：

   ```bash
   python3 -m pip install flake8
   flake8 --version #我们推荐使用3.8.1版本以后的flake8
   cd pyminer/ && flake8 >> flake8_issue_before.log
   #上述操作可以识别出已有代码的问题数量
   #我们要确保新增问题数量为0
   #或者新增问题都具备相应的解释和说明
   #修改完代码后，在提交前先执行如下代码：
   flake8 >> flake8_issue_after.log
   #要保障`flake8_issue_after.log`行数<=`flake8_issue_before.log`行数
   ```

6. 执行你的增删改操作，如果你的修改只涉及一个文件，如修改了`README.md`，则只需要执行：

   ```bash
   git add README.md &&\
   git commit -m '修改了README.md文件，补充了如何贡献模块' &&\
   git push --set-upstream origin modify_readme
   ```

7. 如果不涉及多个commit的情况，则只需要在https://gitee.com/your_name/pyminer/pulls里面直接新建一个Pull Request，源分支选择你新增的特性分支，目标分支按照需求更新到pyminer的`master`分支或者`dev`分支，然后等待评审的回复即可。如果涉及到多个commit，请按照第8步操作继续往下操作，执行变基。

8. 执行变基操作，将多个commit合并为1个：

   ```bash
   #假如你提交了1个特性`feature1`
   git add feature1.py
   git commit -m 'Add feature1'
   git push
   #然后你又想同时提交1个新特性`feature2`，但是还没提交PR
   git add feature2.py
   git commit -m 'Add feature2'
   git push
   #这时候你需要执行变基，将历史的2个commit合并成一个来提交，提升评审效率
   #先查看想要合并的历史commit
   git log --oneline --graph
   #例如合并2个commit
   git rebase --interactive HEAD~2
   #按照指示进行操作，一般用i(insert)进入编辑操作
   #以下示例说明将commit2合并到commit1的场景
   pick commit1 -> pick commit1 #保留commit1
   pick commit2 -> fixup commit2 #去掉commit2的说明内容
   #按esc之后输入`:wq`保存更改
   #最后再强制提交，保存变基的操作，这样变基的流程就结束了，可以回到第6步进行提交
   git push --force
   ```

#### 联系我们

1.  作者：PyMiner Development Team
2.  邮箱：aboutlong@qq.com


#### 许可说明
本项目遵循GPL许可证。此外，对个人用户来说，本项目支持自由修改源码和使用，但是不支持任何未经授权许可的商用或其他盈利性行为，也不支持任何未经授权申请著作权的行为。如有违反以上许可，本项目管理团队有权进行否决。
许可解释权归属 PyMiner Development Team。

#### 预览

基本界面
![avatar](pyminer2/ui/source/screenshot/app2.png)

变量
![avatar](pyminer2/ui/source/screenshot/app2-table.png)

绘图
![avatar](pyminer2/ui/source/screenshot/app2-plot.png)

